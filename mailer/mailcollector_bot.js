/**
 Done! Congratulations on your new bot. You will find it at telegram.me/mailcollector_bot. You can now add a description,
 about section and profile picture for your bot, see /help for a list of commands. By the way, when you've finished
 creating your cool bot, ping our Bot Support if you want a better username for it. Just make sure the bot is fully
 operational before you do this.

 Use this token to access the HTTP API:
 225446655:AAE-gwytXzGQKGZXiVboY_me83JH-ZLt8i4

 For a description of the Bot API, see this page: https://core.telegram.org/bots/api
 */
var self = this;
var express = require('express');
var router = express.Router();

var TelegramBot = require('node-telegram-bot-api');
this.token = '225446655:AAE-gwytXzGQKGZXiVboY_me83JH-ZLt8i4';
// this.token = '242454378:AAFIAFlFQaJ-Bx_2sNWpREvyL8eX0ikyOJk'; //test
this.bot = new TelegramBot(this.token, {polling: true});
this.myEmitter = require('./Events');

var chatId = null;

this.getBot = function () {
    if(chatId != null) {
        return {
            bot: this,
            chatId: chatId
        };
    } else {
        return null;
    }
};

this.bot.on('message', function (msg) {
    var user = {
        id: msg.from.id,
        first_name: msg.from.first_name,
        last_name: msg.from.last_name
    };

    chatId = msg.chat.id;

    var message = {
        text: msg.text,
        date: new Date(msg.date),
        id: msg.message_id
    };

    if(message.text == '/start'){

        this.sendMessage(user.id, "Hello " + user.first_name+"!", {caption: "I'm a bot!"});

        this.onReplyToMessage(user.id, message.id, function (result) {
            this.sendMessage(user.id, "Hello " + user.first_name+"!", {caption: "I'm a bot!"});
        });

        self.myEmitter.emit('end');
        self.myEmitter.emit('start');



    } else if(message.text == '/end'){

        this.sendMessage(user.id, "Пока-пока", {caption: "I'm a bot!"});
        self.myEmitter.emit('end');


    } else {
        this.sendMessage(user.id, "Да работаю я! Работаю!!!", {caption: "I'm a bot!"});
    }

});

module.exports = this;
