var webshot = require ('webshot');


class Body{
    constructor(object, callback){
        this.callback = callback;

        if(object.html){
            this.toPng(object.html);
        } else {
            this.callback('text', object.text);
        }
    }

    toPng(html){

        let options  = {
            siteType:'html'
        };

        let nameFile = Math.random().toString()+'.png';
        console.log('nameFile', nameFile);

        webshot(html, nameFile, options, (err) => {
            this.callback('image', nameFile);
        });
    }
}

module.exports = Body;