
let Class = require('jsface').Class;

let path = require('path');
let fs = require('fs');

let express = require('express');
let router = express.Router();
let bot = require('./mailcollector_bot');

let notifier = require('mail-notifier');
let imaps = require('../mailer/imaps.json');

let Body = require('./Body');


let Notifier = Class({

    constructor: function (imap, bot) {
        let n = notifier(imap);
        this.bot = bot;

        this.handleActions(n);

    },

    handleActions: function (n) {
        let self = this;
        // session closed
        n.on('end', () => { n.start(); }).on('mail',(m) => { self.onMail(m); }).start();
    },

    onMail: function (m) {

        let bbot = this.bot.getBot();
        if(bbot === null) return;

        let from = m.headers.from,
            to   = m.headers.to,
            header = m.subject;

        let str = 'from ' + from + '; to ' + to + '; header ' + header;

        let body = new Body(m, (type, text) => {

            if(type == text){

                str = str + '; ' + text;
                str = str.length > 4090 ? str.substring(0, 4090) : str;

                bbot.bot.bot.sendMessage(bbot.chatId, str, {caption: "I'm a mailcollector_bot!"});

            } else if(type == 'image'){

                let pathToImg = path.resolve('./' + text);

                    bbot.bot.bot.sendPhoto(bbot.chatId, pathToImg, {caption: str}).then(()=> {
                        this.removeFile(pathToImg);
                    });

            }
        });
    },

    removeFile(pathToImg){
        fs.unlink(pathToImg, (err) => {
            if (err) throw err;
            console.log('successfully deleted '+ pathToImg);
        });
    }
});

let startNotification = function () {
    imaps.forEach((imap) => { new Notifier(imap, bot); });
};

bot.myEmitter.on('start', startNotification);


module.exports = router;

